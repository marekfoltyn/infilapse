package marekfoltyn.infilapse;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class MainActivity extends AppCompatActivity implements NetworkSender.EventCallback, Camera.PictureCallback {

    private static final int QUEUE_CAPACITY = 1000;

    // Photo-related variables
    Camera camera;
    boolean running = false;
    long session;
    int timeout = 5;
    long captureCount = 0;
    int countdown = timeout;
    BlockingQueue<Photo> photoQueue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);

    // UI elements
    TextView lblQueue;
    TextView lblConnection;
    FrameLayout layPreview;
    CameraPreview preview;
    RelativeLayout layout;
    FloatingActionButton button;

    // Other
    final Handler handler = new Handler(); // for posting runnables to the main thread
    NetworkSender network;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        layout = (RelativeLayout) findViewById(R.id.layConstraint);
        lblQueue = (TextView) findViewById(R.id.lblQueue);
        lblConnection = (TextView) findViewById(R.id.lblConnection);
        layPreview = (FrameLayout) findViewById(R.id.layPreview);

        button = (FloatingActionButton) findViewById(R.id.fab);
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#00AA00")));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(running)
                {
                    stopTimelapse();
                } else
                {
                    startTimelapse();
                }
            }
        });

        setUiConnected(false);
        network = new NetworkSender(photoQueue, this);

        updateQueueStatus();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        startCamera();
        network.startNetworking();
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        stopTimelapse();
        stopCamera();
        network.stopNetworking();
    }


    private void startCamera()
    {
        if(camera == null)
        {
            try
            {
                camera = Camera.open();
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Camera is not available!", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        // Create our Preview view and set it as the content of our activity.
        preview = new CameraPreview(this, camera);
        layPreview.addView(preview);
    }


    private void stopCamera()
    {
        if(camera != null)
        {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        layPreview.removeAllViews();
    }


    private void startTimelapse()
    {
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#AA0000")));
        button.setImageBitmap(textAsBitmap(String.valueOf(countdown+1), 40f, Color.WHITE));
        session = System.currentTimeMillis();
        countdown = timeout;
        captureCount = 0;
        running = true;
        photoQueue.clear();
        updateQueueStatus();

        // First decrease is ran immediately
        decreaseTimeout();
    }


    private void stopTimelapse()
    {
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#00AA00")));
        button.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
        running = false;
    }

    /**
     * Next second while waiting for next photo
     */
    private void decreaseTimeout()
    {
        if(!running) return;

        if(countdown == 0)
        {
            camera.takePicture(null, null, this);
            countdown = timeout;
        }

        countdown--;

        // +1 is to avoid showing '0' in countdown
        Bitmap textBitmap = textAsBitmap(String.valueOf(countdown+1), 40f, Color.WHITE);
        button.setImageBitmap(textBitmap);

        // Schedule next decrease
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                decreaseTimeout();
            }
        }, 1000);
    }


    /**
     * Camera callback, store taken picture in the queue
     */
    @Override
    public void onPictureTaken(byte[] data, Camera camera)
    {
        Photo photo;
        try{
            photo = new Photo();
            photo.dataJpg = data.clone();
        } catch (OutOfMemoryError error) {
            // Full RAM, discard the photo
            //TODO: temporary save to internal memory?
            Toast.makeText(getApplicationContext(), "Full RAM! Photo discarded.", Toast.LENGTH_LONG).show();
            return;
        }

        photo.session = session;
        photo.timestamp = System.currentTimeMillis();
        photoQueue.add(photo);
        captureCount++;
        updateQueueStatus();
    }


    /**
     * Update UI when number of photos (queued / total) changes
     */
    private void updateQueueStatus()
    {
        int queued = photoQueue.size();
        String text = String.format(Locale.US, "Queued: %d, Total: %d", queued, captureCount);
        lblQueue.setText(text);
    }


    /**
     * Update UI when connection changes
     */
    private void setUiConnected(boolean connected)
    {
        if(connected)
        {
            lblConnection.setText("Connected");
            lblConnection.setTextColor(Color.GREEN);
        }
        else
        {
            lblConnection.setText("Not connected");
            int orange = Color.rgb(255, 153, 0);
            lblConnection.setTextColor(orange);
        }
    }


    /**
     * Update UI when network is broken
     */
    private void setUiNetworkBroken(String message)
    {
        lblConnection.setText(message);
        lblConnection.setTextColor(Color.RED);
    }

    // Get notifications from networking (not running in main thread)

    @Override
    public void onConnected()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run() {
                setUiConnected(true);
            }
        });
    }

    @Override
    public void onDisconnected()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run() {
                setUiConnected(false);
            }
        });
    }

    @Override
    public void onNetworkBroken(String message)
    {
        final String messageFinal = message;
        handler.post(new Runnable() {
            @Override
            public void run() {
                setUiNetworkBroken(messageFinal);
            }
        });
    }

    @Override
    public void onQueueModified()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run() {
                updateQueueStatus();
            }
        });
    }


    /**
     * Helper class that transforms a String into Bitmap
     */
    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }
}
