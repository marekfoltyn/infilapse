package marekfoltyn.infilapse;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

/**
 * Class that takes photos from a queue and sends them to the server using Timelapse protocol
 */
public class NetworkSender {

    private static final int TIMELAPSE_PORT = 14014;
    private BlockingQueue<Photo> photoQueue;
    boolean runThread = true;
    Thread networkThread;

    InetAddress broadcastAddr = null;
    DatagramSocket udpSocket = null;
    Socket tcpSocket = null;
    OutputStream outputStream;
    DatagramPacket pingPacket;
    DatagramPacket pongPacket;

    public NetworkSender(BlockingQueue<Photo> photoQueue)
    {
        this.photoQueue = photoQueue;
        prepare();
    }

    public NetworkSender(BlockingQueue<Photo> photoQueue, @NonNull EventCallback eventCallback)
    {
        this.photoQueue = photoQueue;
        this.eventCallback = eventCallback;
        prepare();
    }

    private void prepare()
    {
        // Prepare broadcast address
        try {
            broadcastAddr = InetAddress.getByName("255.255.255.255");
        } catch (UnknownHostException e) {
            e.printStackTrace();
            eventCallback.onNetworkBroken("Can't createe UDP broadcast address");
            return;
        }

        // Create new UDP socket
        try {
            udpSocket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
            eventCallback.onNetworkBroken("Can't createe UDP socket");
            return;
        }

        // Prepare broadcast packet
        String udpMessage = "timelapse_v1 ";
        byte msgPayload[] = udpMessage.getBytes().clone();
        msgPayload[12] = 0; // null-termination

        pingPacket = new DatagramPacket(
                msgPayload,
                13,
                broadcastAddr,
                TIMELAPSE_PORT
        );

        // Allocate receive packet
        pongPacket = new DatagramPacket(
                new byte[13],
                13
        );
    }


    public void startNetworking()
    {
        runThread = true;
        networkThread = new Thread(new Runnable() {
            @Override
            public void run() {
                networkWorker();
            }
        }, "networkThread");
        networkThread.start();
    }


    public void stopNetworking()
    {
        runThread = false;
        try {
            networkThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void networkWorker()
    {

        // Main networking loop
        while(runThread)
        {
            TcpEndpoint endpoint = findServer();
            if(endpoint == null) continue;

            // Connect to the server
            try {
                tcpSocket = new Socket(endpoint.address, endpoint.port);
                outputStream = tcpSocket.getOutputStream();
            }
            catch (IOException e) {
                // Could not connect to the server, return to UDP searching
                e.printStackTrace();
                continue;
            }

            eventCallback.onConnected();

            // Queue-sending loop
            while(true)
            {

                // Take the photo from the queue
                Photo photo;
                try{
                    photo = photoQueue.take();
                    eventCallback.onQueueModified();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                    continue;
                }
                assert photo != null;


                // Prepare message (header + photo)
                ByteBuffer headerBuffer = ByteBuffer.allocate(20);
                ByteBuffer payloadBuffer = ByteBuffer.allocate(photo.dataJpg.length);

                // Always in big-endian (network byte order)
                headerBuffer.putLong(photo.session);
                headerBuffer.putLong(photo.timestamp);
                headerBuffer.putInt(photo.dataJpg.length);
                payloadBuffer.put(photo.dataJpg);
                try
                {
                    outputStream.write(headerBuffer.array());
                    outputStream.flush();
                    outputStream.write(payloadBuffer.array());
                    outputStream.flush();
                    Log.v("Infilapse", "Sent a photo: " + photo.session + " / " + photo.timestamp + " / " + photo.dataJpg.length);
                } catch (IOException e)
                {
                    // Not sent, return the photo to the queue
                    eventCallback.onDisconnected();
                    photoQueue.add(photo);
                    eventCallback.onQueueModified();
                    Log.e("Infilapse", "Photo not sent, returing into the queue.");
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    /**
     * Find server (IP and port) in LAN
     */
    private TcpEndpoint findServer()
    {
        // Searching loop
        while(runThread)
        {
            try {

                udpSocket.send(pingPacket);
                Log.v("Infilapse", "UDP ping sent.");
                udpSocket.setSoTimeout(1000);
                udpSocket.receive(pongPacket);

                if(Arrays.equals(pingPacket.getData(), pongPacket.getData()))
                {
                    // Server found
                    TcpEndpoint endpoint = new TcpEndpoint();
                    endpoint.address = pongPacket.getAddress();
                    endpoint.port = pongPacket.getPort();
                    return endpoint;
                }
                else
                {
                    Log.w("Infilapse", "Invalid response packet");
                }
            }

            catch (SocketTimeoutException ignore) {
                // No server found, try again
                continue;
            }

            catch (IOException e) {
                e.printStackTrace();

                // Network not available, wait
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                continue;
            }
        }
        return null;
    }

    /**
     * Interface for getting notifications about networking
     */
    public interface EventCallback
    {
        void onConnected();
        void onDisconnected();
        void onNetworkBroken(String message);
        void onQueueModified();
    }

    /**
     * No-op implementation of EventCallback to avoid if(callback == null) before every call
     */
    private EventCallback eventCallback = new EventCallback() {
        @Override
        public void onConnected() {}

        @Override
        public void onDisconnected() {}

        @Override
        public void onNetworkBroken(String message) {}

        @Override
        public void onQueueModified() {}
    };

}


/**
 * Simple structure that describes a network endpoint
 */
class TcpEndpoint
{
    public InetAddress address;
    public int port;
}





