
#include <iostream>
#include <asio.hpp>
#include <ctime>

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::size_t;
using std::bind;
using std::ref;
using std::error_code;
using std::move;
using std::placeholders::_1;
using std::placeholders::_2;
using std::shared_ptr;
using asio::io_service;
using asio::signal_set;
using asio::mutable_buffer;
using asio::const_buffer;
using namespace asio::ip;

#define TIMELAPSE_PORT 14014

char headerBufferData[20];
mutable_buffer headerBuffer(headerBufferData, 20);
string savefolder;


string readableSize(uint64_t bytes)
{
    double bytesDouble = 1.0 * bytes;
    int i = 0;
    const char * units[] = {"B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
    while (bytesDouble > 1024) {
        bytesDouble /= 1024;
        i++;
    }

    char buf[100] = {0};
    sprintf(buf, "%.*f %s", i, bytesDouble, units[i]);
    return string(buf);
}




void udp_handler(
    const std::error_code & error,
    size_t bytes_transferred,
    udp::socket & udpSocket,
    mutable_buffer & udpBuffer,
    udp::endpoint & endpoint
)
{
    // Terminate string
    auto * data = (char *) udpBuffer.data();
    data[12] = 0;
    if(bytes_transferred == 13 && strcmp(data, "timelapse_v1") == 0)
    {
        // Send the response
        error_code ignore;
        udpSocket.send_to(udpBuffer, endpoint, 0, ignore);
    }
    else
    {
        // Invalid UDP packet, discard
        //cerr << bytes_transferred;
        cerr << "Invalid UDP packet received: " << data << endl;
    }

    // UDP loop
    udpSocket.async_receive_from(
        udpBuffer,
        endpoint,
        bind(udp_handler, _1, _2, ref(udpSocket), ref(udpBuffer), ref(endpoint))
    );
}


string readableTime()
{
    std::time_t now = std::time(nullptr);
    struct tm * dt;
    char buffer [30];
    dt = localtime(&now);
    strftime(buffer, sizeof(buffer), "[%d.%m.%Y %H:%M:%S]", dt);
    return std::string(buffer);
}


void tcp_acceptor_handler(const error_code &error, tcp::socket & client, tcp::acceptor & acceptor);

void tcp_receive_handler(const error_code & error, size_t bytes_transferred, tcp::socket & client, tcp::acceptor & acceptor)
{
    if(bytes_transferred > 0 && bytes_transferred != 20)
    {
        // Invalid header size
        cerr << "Invalid header. Closing connection." << endl;
        error_code ec;
        client.close(ec);
    }

    if(
        (error == asio::error::eof) ||               // Client disconnected.
        (asio::error::connection_reset == error)     // Client disconnected

    ){
        cout << "Client disconnected." << endl;
        client.close();
        acceptor.async_accept(client, bind(tcp_acceptor_handler, _1, ref(client), ref(acceptor)));
        return;
    }

    // Parse the header
    uint64_t session;
    uint64_t timestamp;
    uint32_t size;
    memcpy(&session, headerBufferData, 8);
    memcpy(&timestamp, headerBufferData+8, 8);
    memcpy(&size, headerBufferData+16, 4);

    // Convert from network byte order
    session = htonll(session);
    timestamp = htonll(timestamp);
    size = htonl(size);

    cout << "Received header: " << endl;
    cout << "session: " << session << endl;
    cout << "timestamp: " << timestamp << endl;
    cout << "size: " << size << endl;

    // Receive the photo
    std::vector<char> photo((const unsigned int) size);
    mutable_buffer photoBuffer(photo.data(), (size_t) size);

    size_t received = 0;
    error_code errorCode;

    received = asio::read(client, photoBuffer, errorCode);

    if(error)
    {
        cerr << "Error during photo receiving: " << error << endl;
    }

    else if(received != size)
    {
        cerr << "Error: received size does not match header size." << endl;
        cerr << "Received: " << received << ", size: " << size << endl;
    }

    else
    {
        // Save the photo
        string filename = std::to_string(session);
        filename += "_";
        filename += std::to_string(timestamp);
        filename += ".jpg";

        string fullpath(savefolder);
        fullpath += "/";
        fullpath += filename;

        std::FILE * file = fopen(fullpath.c_str(), "w");
        if(!file)
        {
            cerr << "Cannot open file " << fullpath << "! Photo will be discarded." << endl;
        }
        else
        {
            auto written = std::fwrite(photoBuffer.data(), sizeof(char), received, file);
            if(written != received)
            {
                cerr << "Error during photo saving! Written only " << written << "bytes" << endl;
            }
            cout << readableTime() << " Saved photo: " << session << " / " << timestamp << " (" << readableSize(received) << ")" << endl;
        }
        fclose(file);

    }

    // Repeat photo receive
    client.async_receive(headerBuffer, bind(tcp_receive_handler, _1, _2, ref(client), ref(acceptor)));
}


void tcp_acceptor_handler(const error_code &error, tcp::socket & client, tcp::acceptor & acceptor)
{
    cout << "A client connected." << endl;
    client.async_receive(headerBuffer, MSG_WAITALL, bind(tcp_receive_handler, _1, _2, ref(client), ref(acceptor)));
}


int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        cout << endl;
        cout << "Usage: Infilapse <save_folder>" << endl;
        cout << endl;
        //cout << "If no save_folder provided, working directory is used" << endl;
        //cout << endl;
        return 1;
    }

    savefolder = string(argv[1]);
    io_service io;

    // Construct a signal set registered for process termination.
    signal_set signals(io, SIGINT);

    // Start an asynchronous wait for one of the signals to occur.
    signals.async_wait([&](const std::error_code & error, int signal_number)
    {
        cout << endl << "Exiting Infilapse." << endl;
        io.stop();
        exit(0);
    });

    // Set up UDP discovery-listening socket
    char discoveryBufferData[13];
    mutable_buffer discoveryBuffer(discoveryBufferData, 13);
    udp::endpoint udpEndpoint(udp::v4(), TIMELAPSE_PORT);
    udp::socket udpSocket(io, udpEndpoint);
    if(!udpSocket.is_open())
    {
        cerr << "Couldn't open UDP socket on port " << TIMELAPSE_PORT << " ." << endl;
        exit(1);
    }
    udp::endpoint remote_endpoint;
    udpSocket.async_receive_from(
        discoveryBuffer,
        remote_endpoint,
        bind(udp_handler, _1, _2, ref(udpSocket), ref(discoveryBuffer), ref(remote_endpoint))
    );

    // Set up TCP acceptor socket
    tcp::endpoint tcpEndpoint(tcp::v4(), TIMELAPSE_PORT);
    tcp::acceptor tcpAcceptor(io, tcpEndpoint);
    if(!tcpAcceptor.is_open())
    {
        cerr << "Couldn't open TCP socket on port " << TIMELAPSE_PORT << " ." << endl;
        exit(1);
    }
    tcpAcceptor.listen();
    tcp::socket peerSocket(io);
    tcpAcceptor.async_accept(peerSocket, bind(tcp_acceptor_handler, _1, ref(peerSocket), ref(tcpAcceptor)));

    io.run();
}